# README #

A collection of useful functions and tools for working with gravitational waveform data.

# Install #

To install from pypi

```bash
pip install gwtools
```

To install from source

```bash
git clone git@bitbucket.org:chadgalley/gwtools.git
pip install -e gwtools
```

where the "-e" installs an editable (development) project with pip. This allows
your local code edits to be automatically seen by the system-wide installation.
